echo 'descargando secuencias'


if [ "$#" -eq 1 ]
then

    sampleid=$1

    echo "comprobacion calidad..."
    mkdir -p out/fastqc
    fastqc -o out/fastqc data/${sampleid}*.fastq.gz

    echo "Running cutadapt..."
    mkdir -p out/cutadapt
    mkdir -p log/cutadapt
    cutadapt -m 20 -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCA -A AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT -o out/cutadapt/${sampleid}_1.trimmed.fastq.gz -p out/cutadapt/${sampleid}_2.trimmed.fastq.gzdata/${sampleid}.fastq.gz data/${sampleid}.fastq.gz > log/cutadapt/${sampleid}.log

    echo "Running STAR alignment..."
    mkdir -p out/star/${sampleid}
    STAR --runThreadN 4 --genomeDir res/genome/star_index/ --readFilesIn out/cutadapt/${sampleid}_1.trimmed.fastq.gz out/cutadapt/${sampleid}_2.trimmed.fastq.gz --readFilesCommand zcat --   outFileNamePrefix out/star/${sampleid}/


else

   echo "Usage: $0 <sampleid>"
   exit 1
fi
